package Really;

public class Pessoa {
    
    private int id;

    public Pessoa() {
       
    }

    public void setId(int id) {
        this.id = id;
    }
    private String nome;
    private String telefone;
    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public Pessoa(int id, String nome, String telefone) {
        this(nome, telefone);
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone() {
        return this.telefone;
    }

    @Override
    public String toString() {
        
        return "Nome: " + this.nome + " - " 
                + "Telefone: " + this.telefone;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}