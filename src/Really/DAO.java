
package Really;

import java.util.List;


public interface DAO<T> {
    
    void inserir(T objeto);
    void atualizar(T objeto);
    void delete(int id);
    List<T> listarTodos();
    T buscarPorId(int id);
}